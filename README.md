# Cooking Planner

##### Simple cooking planner app for your family.

With cooking planner you can add and store your recipes. On the plan tab you can plan meals which will add all ingredients automatically of the recipe to your grocery list.

## Install

- [F-Droid](https://fdroid.nordgedanken.dev/repos/fcXuoa5GRWGALNiBcfNPYrapHClz-ISn/repo?fingerprint=561D5453BFD5029DD8A9A2BFAA7E2E5872009AB42430731A285B1046A4A1F686)
- [PlayStore](https://play.google.com/store/apps/details?id=io.gitlab.christianpauly.cookingplanner)

## Screenshots

<p>
  <img src="https://gitlab.com/ChristianPauly/cooking_planner/-/raw/master/assets/screenshots/Screenshot_01_cookingplanner.png" width="24%" />
  <img src="https://gitlab.com/ChristianPauly/cooking_planner/-/raw/master/assets/screenshots/Screenshot_02_cookingplanner.png" width="24%" />
  <img src="https://gitlab.com/ChristianPauly/cooking_planner/-/raw/master/assets/screenshots/Screenshot_03_cookingplanner.png" width="24%" />
  <img src="https://gitlab.com/ChristianPauly/cooking_planner/-/raw/master/assets/screenshots/Screenshot_04_cookingplanner.png" width="24%" />
</p>

## Install

-  [F-Droid](https://fdroid.nordgedanken.dev/repos/fcXuoa5GRWGALNiBcfNPYrapHClz-ISn/repo?fingerprint=561D5453BFD5029DD8A9A2BFAA7E2E5872009AB42430731A285B1046A4A1F686)

## How to build

1. [Install flutter](https://flutter.dev)

2. Clone the repo:
```
git clone https://gitlab.com/ChristianPauly/cooking_planner
cd cooking_planner
```

3. `flutter run`

## How to add translations for your language

1. Replace the non-translated string in the codebase:
```
Text("Hello world"),
```
with a method call:
```
Text(I18n.of(context).helloWorld),
```
And add the method to `/lib/i18n/i18n.dart`:
```
String get helloWorld => Intl.message('Hello world');
```

2. Add the string to the .arb files with this command:
```
flutter pub run intl_translation:extract_to_arb --output-dir=lib/i18n lib/i18n/i18n.dart
```

3. Copy the new translation objects from `/lib/i18n/intl_message.arb` to `/lib/i18n/intl_<yourlanguage>.arb` and translate it or create a new file for your language by copying `intl_message.arb`.

4. Update the translations with this command:
```
flutter pub pub run intl_translation:generate_from_arb --output-dir=lib/i18n --no-use-deferred-loading lib/i18n/i18n.dart lib/i18n/intl_*.arb
```

5. Make sure your language is in `supportedLocales` in `/lib/main.dart`.
