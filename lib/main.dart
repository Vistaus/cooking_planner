/* Cooking Planner
* Copyright (C) 2020  Christian Pauly
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* 
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import 'package:cooking_planner/views/recipes_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';

import 'config/cooking_planner_colors.dart';
import 'i18n/i18n.dart';

void main() => runApp(CookingPlannerApp());

class CookingPlannerApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StyledToast(
      locale: Locale('en'),
      toastPositions: StyledToastPosition.top,
      child: MaterialApp(
        title: 'Cooking Planner',
        theme: ThemeData(
          primaryColor: CookingPlannerColors.primaryColor,
          accentColor: CookingPlannerColors.accentColor,
          cardTheme: CardTheme(
            elevation: 4,
            margin: EdgeInsets.all(8),
          ),
        ),
        darkTheme: ThemeData.dark(),
        localizationsDelegates: [
          AppLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en'), // English
          const Locale('de'), // German
        ],
        home: RecipesView(),
      ),
    );
  }
}
