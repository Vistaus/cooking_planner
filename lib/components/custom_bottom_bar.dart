/* Cooking Planner
* Copyright (C) 2020  Christian Pauly
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* 
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import 'package:cooking_planner/i18n/i18n.dart';
import 'package:cooking_planner/utils/fade_route.dart';
import 'package:cooking_planner/views/grocery_list_view.dart';
import 'package:cooking_planner/views/plan_view.dart';
import 'package:cooking_planner/views/recipes_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomBottomBar extends StatelessWidget {
  final int currentIndex;

  const CustomBottomBar(this.currentIndex);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      onTap: (int i) {
        if (i == currentIndex) return;
        Widget newView;
        switch (i) {
          case 0:
            newView = RecipesView();
            break;
          case 1:
            newView = PlanView();
            break;
          case 2:
            newView = GroceryListView();
            break;
        }

        Navigator.of(context).pushReplacement(
          FadeRoute(
            builder: (c) => newView,
          ),
        );
      },
      currentIndex: currentIndex,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.fastfood),
          title: Text(I18n.of(context).recipes),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.event),
          title: Text(I18n.of(context).mealPlan),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.list),
          title: Text(I18n.of(context).groceryList),
        ),
      ],
    );
  }
}
