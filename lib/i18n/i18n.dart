/* Cooking Planner
* Copyright (C) 2020  Christian Pauly
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* 
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'messages_all.dart';

class AppLocalizationsDelegate extends LocalizationsDelegate<I18n> {
  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return ['en', 'de'].contains(locale.languageCode);
  }

  @override
  Future<I18n> load(Locale locale) {
    return I18n.load(locale);
  }

  @override
  bool shouldReload(LocalizationsDelegate<I18n> old) {
    return false;
  }
}

class I18n {
  I18n(this.localeName);

  String get plan => Intl.message('Plan');

  String get recipes => Intl.message('Recipes');

  String get groceryList => Intl.message('Grocery List');

  String get settings => Intl.message('Settings');

  String get edit => Intl.message('Edit');

  String get newRecipe => Intl.message('New recipe');

  String get close => Intl.message('Close');

  String get areYouSure => Intl.message('Are you sure?');

  String get confirm => Intl.message('Confirm');

  String get loadingPleaseWait => Intl.message('Loading... Please wait.');

  String get mealPlan => Intl.message('Meal plan');

  String get newEntry => Intl.message('New entry');

  String get search => Intl.message('Search');

  String get about => Intl.message('About');

  String get help => Intl.message('Help');

  String get license => Intl.message('License');

  String get sourceCode => Intl.message('Source code');

  String get nextcloudSynchronization =>
      Intl.message('Nextcloud synchronisation');

  String get server => Intl.message('Server');

  String get connect => Intl.message('Connect');

  String get username => Intl.message('Username');

  String get password => Intl.message('Password');

  String get nextcloudConnected => Intl.message('Nextcloud connected');

  String get addSomeRecipesBy =>
      Intl.message('Add some recipes by tapping on the + button.');

  String get addSomeItemsBy =>
      Intl.message('Add some items by tapping on the + button.');

  String get recipeTitle => Intl.message('Recipe title');

  String get instructions => Intl.message('Instructions');

  String get ingredients => Intl.message('Ingredients (One per line)');

  get ingredientsExample => Intl.message('3 Tomatos\n10 Potatos\n12 Bananas');

  String get pleaseEnterTitle =>
      Intl.message('Please enter a title for the receipt');

  String get sunday => Intl.message('Sunday');

  String get saturday => Intl.message('Saturday');

  String get friday => Intl.message('Friday');

  String get monday => Intl.message('Monday');

  String get tuesday => Intl.message('Tuesday');

  String get wednesday => Intl.message('Wednesday');

  String get thursday => Intl.message('Thursday');

  String get addMeal => Intl.message('Add meal');

  String get needOneTimePasswordHint =>
      Intl.message('You may need to create a one time password for this app.');

  String get remove => Intl.message('Remove');

  String countIngredients(String count) => Intl.message(
        '$count ingredients',
        name: 'countIngredients',
        args: [count],
      );

  static Future<I18n> load(Locale locale) {
    final String name =
        locale.countryCode == null ? locale.languageCode : locale.toString();
    final String localeName = Intl.canonicalizedLocale(name);

    return initializeMessages(localeName).then((bool _) {
      Intl.defaultLocale = localeName;
      return I18n(localeName);
    });
  }

  static I18n of(BuildContext context) {
    return Localizations.of<I18n>(context, I18n);
  }

  final String localeName;

  String formatDate(String year, String month, String day, String weekDay) =>
      Intl.message(
        '$weekDay ($year-$month-$day)',
        name: 'formatDate',
        args: [
          year,
          month,
          day,
          weekDay,
        ],
      );
}
