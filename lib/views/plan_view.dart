/* Cooking Planner
* Copyright (C) 2020  Christian Pauly
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* 
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import 'package:cooking_planner/components/custom_bottom_bar.dart';
import 'package:cooking_planner/components/dialogs/simple_dialogs.dart';
import 'package:cooking_planner/i18n/i18n.dart';
import 'package:cooking_planner/model/cooking_plannert.dart';
import 'package:cooking_planner/model/meal.dart';
import 'package:cooking_planner/views/recipe_chooser_view.dart';
import 'package:cooking_planner/views/settings_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PlanView extends StatelessWidget {
  void _deleteMealAction(BuildContext context, String mealId) async {
    await SimpleDialogs(context).tryRequestWithLoadingDialog(
      CookingPlanner().removeMeal(mealId),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(I18n.of(context).plan),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () => Navigator.of(context).push(
              CupertinoPageRoute(
                builder: (c) => SettingsView(),
              ),
            ),
          )
        ],
      ),
      body: FutureBuilder<bool>(
        future: CookingPlanner().isReady,
        builder: (BuildContext context, snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }
          final now = DateTime.now();
          return RefreshIndicator(
            onRefresh: () async {
              await CookingPlanner().refreshFromNextcloud();
              return;
            },
            child: StreamBuilder<Object>(
                stream: CookingPlanner().onMealsUpdate.stream,
                builder: (context, snapshot) {
                  return ListView.separated(
                      separatorBuilder: (BuildContext context, int i) =>
                          Divider(),
                      itemCount: 100,
                      itemBuilder: (BuildContext context, int i) {
                        final day = DateTime.fromMillisecondsSinceEpoch(
                          now.millisecondsSinceEpoch +
                              (i * 1000 * 60 * 60 * 24),
                        );
                        String weekDay;
                        switch (day.weekday) {
                          case 1:
                            weekDay = I18n.of(context).monday;
                            break;
                          case 2:
                            weekDay = I18n.of(context).tuesday;
                            break;
                          case 3:
                            weekDay = I18n.of(context).wednesday;
                            break;
                          case 4:
                            weekDay = I18n.of(context).thursday;
                            break;
                          case 5:
                            weekDay = I18n.of(context).friday;
                            break;
                          case 6:
                            weekDay = I18n.of(context).saturday;
                            break;
                          case 7:
                            weekDay = I18n.of(context).sunday;
                            break;
                        }
                        List<Meal> mealsToday =
                            List<Meal>.from(CookingPlanner().meals);
                        mealsToday.removeWhere((m) =>
                            m.date.year != day.year ||
                            m.date.month != day.month ||
                            m.date.day != day.day);
                        return Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            ListTile(
                              title: Text(
                                I18n.of(context).formatDate(
                                  day.year.toString(),
                                  day.month.toString().padLeft(2, '0'),
                                  day.day.toString().padLeft(2, '0'),
                                  weekDay,
                                ),
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Theme.of(context).accentColor,
                                ),
                              ),
                            ),
                            for (var i = 0; i < mealsToday.length; i++)
                              Card(
                                child: ListTile(
                                  title: Text(CookingPlanner()
                                      .getRecipeById(mealsToday[i].recipeId)
                                      .title),
                                  subtitle: Text(
                                    I18n.of(context).countIngredients(
                                        CookingPlanner()
                                            .getRecipeById(
                                                mealsToday[i].recipeId)
                                            .ingredients
                                            .length
                                            .toString()),
                                  ),
                                  leading: Icon(Icons.fastfood),
                                  trailing: IconButton(
                                    icon: Icon(Icons.delete_outline),
                                    onPressed: () => _deleteMealAction(
                                        context, mealsToday[i].id),
                                  ),
                                ),
                              ),
                            ListTile(
                              leading: Icon(Icons.add),
                              title: Text(I18n.of(context).addMeal),
                              onTap: () => Navigator.of(context).push(
                                CupertinoPageRoute(
                                  builder: (c) => RecipeChooserView(day),
                                ),
                              ),
                            ),
                          ],
                        );
                      });
                }),
          );
        },
      ),
      bottomNavigationBar: CustomBottomBar(1),
    );
  }
}
