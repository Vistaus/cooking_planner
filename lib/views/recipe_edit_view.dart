/* Cooking Planner
* Copyright (C) 2020  Christian Pauly
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* 
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import 'package:cooking_planner/components/dialogs/simple_dialogs.dart';
import 'package:cooking_planner/i18n/i18n.dart';
import 'package:cooking_planner/model/cooking_plannert.dart';
import 'package:cooking_planner/model/recipe.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';

class RecipeEditView extends StatefulWidget {
  final String recipeId;

  RecipeEditView({this.recipeId});

  @override
  _RecipeEditViewState createState() => _RecipeEditViewState();
}

class _RecipeEditViewState extends State<RecipeEditView> {
  bool get editMode => widget.recipeId != null;

  void _saveAction(BuildContext context) async {
    if (_titleController.text?.isEmpty ?? true) {
      showToast(I18n.of(context).pleaseEnterTitle);
      return;
    }
    bool success;
    final recipe = Recipe(
      title: _titleController.text,
      description: _instructionsController.text,
      ingredients: ingredients,
    );
    if (editMode) {
      recipe.id = CookingPlanner()
          .recipes
          .firstWhere((r) => r.id == widget.recipeId)
          .id;
      success = await SimpleDialogs(context).tryRequestWithLoadingDialog(
        CookingPlanner().editRecipe(recipe.id, recipe),
      );
    } else {
      recipe.id = recipe.title + DateTime.now().toString();
      success = await SimpleDialogs(context).tryRequestWithLoadingDialog(
        CookingPlanner().addRecipe(recipe),
      );
    }
    if (success != false) {
      Navigator.of(context).pop();
    }
  }

  void _deleteAction(BuildContext context) async {
    final success = await SimpleDialogs(context).tryRequestWithLoadingDialog(
      CookingPlanner().removeRecipe(widget.recipeId),
    );

    if (success != false) {
      Navigator.of(context).pop();
    }
  }

  void _addIngredientAction() {
    setState(
      () => ingredients.add(_ingredientsController.text),
    );
    _ingredientsController.clear();
    _ingredientsFocus.requestFocus();
  }

  final TextEditingController _titleController = TextEditingController();

  final TextEditingController _ingredientsController = TextEditingController();

  final TextEditingController _instructionsController = TextEditingController();

  final FocusNode _ingredientsFocus = FocusNode();

  List<String> ingredients;

  @override
  Widget build(BuildContext context) {
    Recipe recipe;
    if (editMode) {
      recipe =
          CookingPlanner().recipes.firstWhere((r) => r.id == widget.recipeId);
      _titleController.text = recipe.title;
      _instructionsController.text = recipe.description;
      ingredients ??= List<String>.from(recipe.ingredients);
    }
    ingredients ??= [];
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          editMode ? I18n.of(context).edit : I18n.of(context).newRecipe,
        ),
        actions: <Widget>[
          if (editMode)
            IconButton(
              icon: Icon(Icons.delete_outline),
              onPressed: () => _deleteAction(context),
            ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.check),
        onPressed: () => _saveAction(context),
      ),
      body: Card(
        child: ListView(
          padding: EdgeInsets.all(16),
          children: <Widget>[
            TextField(
              controller: _titleController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: I18n.of(context).recipeTitle,
              ),
            ),
            Divider(thickness: 1, height: 32),
            TextField(
              focusNode: _ingredientsFocus,
              controller: _ingredientsController,
              onChanged: (s) => setState(() => null),
              textInputAction: TextInputAction.done,
              onSubmitted: _ingredientsController.text.isEmpty
                  ? null
                  : (s) => _addIngredientAction(),
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                alignLabelWithHint: true,
                labelText: I18n.of(context).ingredients,
                hintText: I18n.of(context).ingredientsExample,
                suffixIcon: IconButton(
                    icon: Icon(Icons.add),
                    onPressed: _ingredientsController.text.isEmpty
                        ? null
                        : _addIngredientAction),
              ),
            ),
            if (ingredients.isNotEmpty) SizedBox(height: 16),
            Column(
              mainAxisSize: MainAxisSize.min,
              children: ingredients
                  .map(
                    (String text) => Card(
                      margin: EdgeInsets.symmetric(vertical: 4),
                      child: ListTile(
                        title: Text(text),
                        trailing: RaisedButton(
                          color: Theme.of(context).accentColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(7),
                          ),
                          child: Icon(Icons.delete_forever),
                          onPressed: () => setState(
                            () => ingredients.removeWhere((s) => s == text),
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList(),
            ),
            Divider(thickness: 1, height: 32),
            TextField(
              controller: _instructionsController,
              minLines: 6,
              maxLines: 12,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                alignLabelWithHint: true,
                labelText: I18n.of(context).instructions,
              ),
            ),
            SizedBox(height: 16),
          ],
        ),
      ),
    );
  }
}
