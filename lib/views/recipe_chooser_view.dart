/* Cooking Planner
* Copyright (C) 2020  Christian Pauly
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* 
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import 'package:cooking_planner/components/dialogs/simple_dialogs.dart';
import 'package:cooking_planner/i18n/i18n.dart';
import 'package:cooking_planner/model/cooking_plannert.dart';
import 'package:cooking_planner/model/meal.dart';
import 'package:flutter/material.dart';

class RecipeChooserView extends StatefulWidget {
  final DateTime day;

  const RecipeChooserView(this.day);
  @override
  _RecipeChooserViewState createState() => _RecipeChooserViewState();
}

class _RecipeChooserViewState extends State<RecipeChooserView> {
  void _selectAction(BuildContext context, String recipeId) async {
    final success = await SimpleDialogs(context).tryRequestWithLoadingDialog(
      CookingPlanner().addMeal(
        Meal(
            id: DateTime.now().millisecondsSinceEpoch.toString(),
            recipeId: recipeId,
            date: widget.day),
      ),
    );
    if (success != false) {
      Navigator.of(context).pop();
    }
  }

  String searchText = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          height: 44,
          margin: EdgeInsets.all(8),
          padding: EdgeInsets.all(8),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: Colors.white.withOpacity(0.66),
          ),
          child: TextField(
            onChanged: (text) => setState(() => searchText = text),
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(10),
              prefixIcon: Icon(Icons.search),
              hintText: I18n.of(context).search,
              border: InputBorder.none,
            ),
          ),
        ),
      ),
      body: ListView.builder(
        itemCount: CookingPlanner().recipes.length,
        itemBuilder: (BuildContext context, int i) => Card(
          child: ListTile(
            title: Text(CookingPlanner().recipes[i].title),
            subtitle: Text(
              I18n.of(context).countIngredients(
                CookingPlanner().recipes[i].ingredients.length.toString(),
              ),
            ),
            onTap: () => _selectAction(context, CookingPlanner().recipes[i].id),
          ),
        ),
      ),
    );
  }
}
