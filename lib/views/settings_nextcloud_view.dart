import 'package:cooking_planner/components/dialogs/simple_dialogs.dart';
import 'package:cooking_planner/i18n/i18n.dart';
import 'package:cooking_planner/model/cooking_plannert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';

class SettingsNextcloudView extends StatelessWidget {
  final TextEditingController serverController = TextEditingController(
      text: CookingPlanner().nextcloudCredentials?.server);
  final TextEditingController usernameController = TextEditingController(
      text: CookingPlanner().nextcloudCredentials?.username);
  final TextEditingController passwordController = TextEditingController(
      text: CookingPlanner().nextcloudCredentials?.password);

  void _connectNextcloudAction(BuildContext context) async {
    if (await SimpleDialogs(context).tryRequestWithLoadingDialog(
          CookingPlanner().setNextcloud(
            serverController.text,
            usernameController.text,
            passwordController.text,
          ),
        ) !=
        false) {
      showToast(I18n.of(context).nextcloudConnected);
      Navigator.of(context).pop();
    }
  }

  void _deleteNextcloudConnection(BuildContext context) async {
    await CookingPlanner().deleteNextcloudConnection();
    serverController.text = '';
    usernameController.text = '';
    passwordController.text = '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(I18n.of(context).nextcloudSynchronization),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.delete_outline),
            onPressed: () => _deleteNextcloudConnection(context),
          ),
        ],
      ),
      body: Card(
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                autocorrect: false,
                controller: serverController,
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.domain),
                  prefixText: 'https://',
                  border: OutlineInputBorder(),
                  labelText: I18n.of(context).server,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                autocorrect: false,
                controller: usernameController,
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.person),
                  border: OutlineInputBorder(),
                  labelText: I18n.of(context).username,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextField(
                autocorrect: false,
                controller: passwordController,
                obscureText: true,
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.security),
                  border: OutlineInputBorder(),
                  labelText: I18n.of(context).password,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: RaisedButton(
                color: Theme.of(context).accentColor,
                child: Text(
                  I18n.of(context).connect.toUpperCase(),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                onPressed: () => _connectNextcloudAction(context),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                I18n.of(context).needOneTimePasswordHint,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
