/* Cooking Planner
* Copyright (C) 2020  Christian Pauly
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* 
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import 'package:cooking_planner/components/custom_bottom_bar.dart';
import 'package:cooking_planner/components/dialogs/simple_dialogs.dart';
import 'package:cooking_planner/i18n/i18n.dart';
import 'package:cooking_planner/model/cooking_plannert.dart';
import 'package:cooking_planner/model/grocery_list_item.dart';
import 'package:cooking_planner/views/settings_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GroceryListView extends StatefulWidget {
  @override
  _GroceryListViewState createState() => _GroceryListViewState();
}

class _GroceryListViewState extends State<GroceryListView> {
  void _addTaskAction() async {
    final task = await SimpleDialogs(context).enterText(
      titleText: I18n.of(context).newEntry,
      labelText: I18n.of(context).newEntry,
    );
    if (task?.isNotEmpty ?? false) {
      await SimpleDialogs(context).tryRequestWithLoadingDialog(
          CookingPlanner().addGroceryListItem(GroceryListItem(
        title: task,
        check: false,
      )));
      setState(() => null);
    }
  }

  void _toggleTaskAction(int i) async {
    await SimpleDialogs(context)
        .tryRequestWithLoadingDialog(CookingPlanner().toggleGroceryListItem(i));
    setState(() => null);
  }

  void _cleanUpAction() async {
    await SimpleDialogs(context)
        .tryRequestWithLoadingDialog(CookingPlanner().cleanUpGroceryList());
    setState(() => null);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(I18n.of(context).groceryList),
        leading: IconButton(
          icon: Icon(Icons.delete_outline),
          onPressed: _cleanUpAction,
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () => Navigator.of(context).push(
              CupertinoPageRoute(
                builder: (c) => SettingsView(),
              ),
            ),
          )
        ],
      ),
      body: FutureBuilder<bool>(
        future: CookingPlanner().isReady,
        builder: (BuildContext context, snapshot) {
          if (!snapshot.hasData) {
            return Center(child: CircularProgressIndicator());
          }
          final groceryList = CookingPlanner().groceryList;
          return RefreshIndicator(
            onRefresh: () async {
              await CookingPlanner().refreshFromNextcloud();
              setState(() => null);
              return;
            },
            child: groceryList.isEmpty
                ? Center(child: Text(I18n.of(context).addSomeItemsBy))
                : ListView.builder(
                    itemCount: groceryList.length,
                    itemBuilder: (BuildContext context, int i) => Opacity(
                      opacity: groceryList[i].check ? 0.5 : 1,
                      child: Card(
                        child: CheckboxListTile(
                          controlAffinity: ListTileControlAffinity.leading,
                          title: Text(groceryList[i].title),
                          value: groceryList[i].check,
                          onChanged: (b) => _toggleTaskAction(i),
                        ),
                      ),
                    ),
                  ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: _addTaskAction,
      ),
      bottomNavigationBar: CustomBottomBar(2),
    );
  }
}
