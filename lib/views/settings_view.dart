/* Cooking Planner
* Copyright (C) 2020  Christian Pauly
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* 
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import 'package:cooking_planner/i18n/i18n.dart';
import 'package:cooking_planner/views/settings_nextcloud_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(I18n.of(context).settings),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            title: Text(I18n.of(context).nextcloudSynchronization),
            trailing: Icon(Icons.cloud),
            onTap: () => Navigator.of(context).push(
              CupertinoPageRoute(
                builder: (c) => SettingsNextcloudView(),
              ),
            ),
          ),
          ListTile(
            trailing: Icon(Icons.help),
            title: Text(I18n.of(context).help),
            onTap: () => launch(
                "https://gitlab.com/ChristianPauly/cooking_planner/issues"),
          ),
          ListTile(
            trailing: Icon(Icons.link),
            title: Text(I18n.of(context).license),
            onTap: () => launch(
                "https://gitlab.com/ChristianPauly/cooking_planner/raw/master/LICENSE"),
          ),
          ListTile(
            trailing: Icon(Icons.code),
            title: Text(I18n.of(context).sourceCode),
            onTap: () =>
                launch("https://gitlab.com/ChristianPauly/cooking_planner"),
          ),
        ],
      ),
    );
  }
}
