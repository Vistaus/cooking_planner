/* Cooking Planner
* Copyright (C) 2020  Christian Pauly
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU Affero General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Affero General Public License for more details.
* 
* You should have received a copy of the GNU Affero General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:cooking_planner/config/cooking_planner_config.dart';
import 'package:cooking_planner/model/grocery_list_item.dart';
import 'package:cooking_planner/model/nextcloud_credentials.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_styled_toast/flutter_styled_toast.dart';
import 'package:nextcloud/nextcloud.dart';

import 'meal.dart';
import 'recipe.dart';

class CookingPlanner {
  static final CookingPlanner _instance = CookingPlanner._internal();
  factory CookingPlanner() => _instance;

  CookingPlanner._internal() {
    isReady = _load();
  }

  List<Recipe> get recipes => _recipes;
  List<Meal> get meals => _meals;
  List<GroceryListItem> get groceryList => _groceryList;

  List<Recipe> _recipes;
  List<Meal> _meals;
  List<GroceryListItem> _groceryList;

  final StreamController onRecipesUpdate = StreamController.broadcast();
  final StreamController onMealsUpdate = StreamController.broadcast();
  final StreamController onGroceryListUpdate = StreamController.broadcast();

  Future<bool> isReady;

  NextcloudCredentials get nextcloudCredentials => _nextcloudCredentials;

  NextcloudCredentials _nextcloudCredentials;
  NextCloudClient get _nextCloudClient => _nextcloudCredentials.nextCloudClient;
  FlutterSecureStorage _flutterSecureStorage = FlutterSecureStorage();

  Future<bool> setNextcloud(
      String server, String username, String password) async {
    final nextcloudCredentials = NextcloudCredentials(
      server: server,
      username: username,
      password: password,
    );
    final nextCloudClient = nextcloudCredentials.nextCloudClient;
    await nextCloudClient.shares.getShares();
    await _flutterSecureStorage.write(
      key: CookingPlannerConfig.nextcloudCredentialsStorageKey,
      value: json.encode(nextcloudCredentials.toJson()),
    );
    _nextcloudCredentials = nextcloudCredentials;
    await refreshFromNextcloud();
    return true;
  }

  Future<void> deleteNextcloudConnection() async {
    await _flutterSecureStorage.delete(
      key: CookingPlannerConfig.nextcloudCredentialsStorageKey,
    );
    _nextcloudCredentials = null;
    return;
  }

  Future<void> refreshFromNextcloud() async {
    try {
      final content = await _nextCloudClient.webDav
          .download(CookingPlannerConfig.nextcloudPath);
      restoreFromJson(json.decode(String.fromCharCodes(content)));
      onMealsUpdate.add(null);
      onGroceryListUpdate.add(null);
      onRecipesUpdate.add(null);
      return true;
    } catch (e) {
      showToast(e.toString());
    }
    return false;
  }

  Future<void> addRecipe(Recipe recipe) async {
    _tempJsonBackup = toJson();
    _recipes.add(recipe);
    _recipes.sort((a, b) => a.title.compareTo(b.title));
    await _save();
    onRecipesUpdate.add(null);
    return;
  }

  Future<void> removeRecipe(String recipeId) async {
    _tempJsonBackup = toJson();
    _recipes.removeWhere((Recipe recipe) => recipe.id == recipeId);
    _meals.removeWhere((Meal meal) => meal.recipeId == recipeId);
    await _save();
    onRecipesUpdate.add(null);
    return;
  }

  Future<void> editRecipe(String recipeId, Recipe recipe) async {
    _tempJsonBackup = toJson();
    final index =
        _recipes.lastIndexWhere((Recipe recipe) => recipe.id == recipeId);
    _recipes[index] = recipe;
    await _save();
    onRecipesUpdate.add(null);
    return;
  }

  Recipe getRecipeById(String recipeId) =>
      recipes.firstWhere((r) => r.id == recipeId);

  Future<void> addMeal(Meal meal) async {
    _tempJsonBackup = toJson();
    _meals.add(meal);
    final recipe = getRecipeById(meal.recipeId);
    for (final ingredient in recipe.ingredients) {
      _addGroceryListItem(ingredient);
    }
    await _save();
    onMealsUpdate.add(null);
    return;
  }

  Future<void> removeMeal(String mealId) async {
    _tempJsonBackup = toJson();
    _meals.removeWhere((Meal meal) => meal.id == mealId);
    await _save();
    onMealsUpdate.add(null);
    return;
  }

  void _addGroceryListItem(String item) {
    final index = _groceryList.indexWhere(
      (g) =>
          g.check == false &&
          (g.title == item ||
              (_isNumeric(g.title.split(' ').first) &&
                  g.title == '${g.title.split(' ').first} $item')),
    );
    if (index != -1) {
      if (_isNumeric(_groceryList[index].title.split(' ').first) &&
          _groceryList[index].title ==
              '${_groceryList[index].title.split(' ').first} $item') {
        var words = _groceryList[index].title.split(' ');
        var counter = int.parse(words.first);
        counter++;
        words.first = counter.toString();
        _groceryList[index].title = words.join(' ');
      } else {
        _groceryList[index].title = '2 ${_groceryList[index].title}';
      }
    } else {
      _groceryList.add(GroceryListItem(title: item, check: false));
    }
  }

  bool _isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return int.tryParse(s) != null;
  }

  Future<void> addGroceryListItem(GroceryListItem item) async {
    _tempJsonBackup = toJson();
    _addGroceryListItem(item.title);
    await _save();
    onGroceryListUpdate.add(null);
    return;
  }

  Future<void> cleanUpGroceryList() async {
    _tempJsonBackup = toJson();
    groceryList.removeWhere((i) => i.check);
    await _save();
    onGroceryListUpdate.add(null);
    return;
  }

  Future<void> toggleGroceryListItem(int index) async {
    _tempJsonBackup = toJson();
    _groceryList[index].check = !_groceryList[index].check;
    await _save();
    onGroceryListUpdate.add(null);
    return;
  }

  Future<bool> _load() async {
    final nextcloudCredentials = await _loadNextcloudCredentials();
    if (nextcloudCredentials == null) {
      final jsonString = await _flutterSecureStorage.read(
          key: CookingPlannerConfig.storageKey);
      if (jsonString != null) {
        try {
          restoreFromJson(json.decode(jsonString));
          return true;
        } catch (e) {
          debugPrint('Secure storage corrupted. Clear the storage! Error: $e');
          await _flutterSecureStorage.deleteAll();
        }
      }
    } else {
      _nextcloudCredentials = nextcloudCredentials;
      await refreshFromNextcloud();
      return true;
    }
    _recipes = List<Recipe>();
    _meals = List<Meal>();
    _groceryList = List<GroceryListItem>();
    return false;
  }

  Map<String, dynamic> _tempJsonBackup;

  Future<void> _save() async {
    try {
      if (_nextcloudCredentials == null) {
        await _flutterSecureStorage.write(
            key: CookingPlannerConfig.storageKey, value: json.encode(toJson()));
      } else {
        final backupString = json.encode(toJson());
        final backupIntList = backupString.codeUnits;
        final backupUint8List = Uint8List.fromList(backupIntList);
        await _nextCloudClient.webDav
            .upload(backupUint8List, CookingPlannerConfig.nextcloudPath);
      }
    } catch (_) {
      restoreFromJson(_tempJsonBackup);
      rethrow;
    }
    return false;
  }

  Future<NextcloudCredentials> _loadNextcloudCredentials() async {
    final jsonString = await _flutterSecureStorage.read(
        key: CookingPlannerConfig.nextcloudCredentialsStorageKey);
    if (jsonString == null) return null;
    try {
      return NextcloudCredentials.fromJson(json.decode(jsonString));
    } catch (e) {
      debugPrint('Secure storage corrupted. Clear the storage! Error: $e');
      await _flutterSecureStorage.deleteAll();
    }
    return null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['recipes'] = List<dynamic>();
    for (final recipe in recipes) {
      data['recipes'].add(recipe.toJson());
    }
    final now = DateTime.now().millisecondsSinceEpoch - (1000 * 60 * 60 * 24);
    _meals.removeWhere((m) => m.date.millisecondsSinceEpoch < now);
    data['meals'] = List<dynamic>();
    for (final meal in meals) {
      data['meals'].add(meal.toJson());
    }
    data['groceryList'] = List<dynamic>();
    for (final groceryListItem in groceryList) {
      data['groceryList'].add(groceryListItem.toJson());
    }
    return data;
  }

  void restoreFromJson(Map<String, dynamic> payload) {
    _recipes = List<Recipe>();
    for (final recipePayload in payload['recipes']) {
      _recipes.add(Recipe.fromJson(recipePayload));
    }
    _meals = List<Meal>();
    for (final mealPayload in payload['meals']) {
      _meals.add(Meal.fromJson(mealPayload));
    }
    _groceryList = List<GroceryListItem>();
    for (final groceryListItemPayload in payload['groceryList']) {
      _groceryList.add(GroceryListItem.fromJson(groceryListItemPayload));
    }
  }
}
